/**
 * @author Sirko Höer
 * @author Lennart Haas (parts marked)
 */

#ifndef CORE_PRAGMAREADERACTION_H
#define CORE_PRAGMAREADERACTION_H


#include <iostream>
#include <string>
#include <sstream>
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Rewrite/Frontend/Rewriters.h>

#include "../pragmahandlers/FuzzPragmaHandler.h"
#include "../preprocessor/PPContext.h"

using namespace clang;
using namespace llvm;
using namespace std;



/// @brief class inherited from RecursiveASTVisitor and travers through the AST
class PragmaReaderVisitor : public RecursiveASTVisitor<PragmaReaderVisitor> {
private:
	/// @brief private variable used to search for the starting position of elements.
	FuzzPragmaHandler *pragmaHandler;	// Line added by Lennart Haas

public:
	PragmaReaderVisitor(FuzzPragmaHandler *pragmaHandler)
			: pragmaHandler(pragmaHandler) {}	// pragmaHandler added to constructor by Lennart Haas

	/// VisitStmt function, travers through the AST-Statements
	/// @param Stmt Pointer
	/// @return bool
	bool VisitStmt(Stmt *s);

	/// VisitFunctionDecl function, travers through the function in a AST
	/// @param FunctionDecl Pointer
	/// @return bool
	bool VisitFunctionDecl(FunctionDecl *f);

	/// VisitVarDecl function, travers through the var declaration in a AST
	/// @param VarDecl Pointer
	/// @return bool
	bool VisitVarDecl(VarDecl *v);
};



class PragmaReaderASTConsumer : public ASTConsumer {
private:
	PragmaReaderVisitor Visitor;

public:
	/// Constructor of class traversASTTree
	/// @param Object Rewriter
	/// @param String startposition
	/// @return this object
	PragmaReaderASTConsumer(FuzzPragmaHandler *pragmaHandler)
			: Visitor(pragmaHandler) {}


	/// HandleTopLevelDecl is entrypoint to start traversing through the AST
	/// @param DeclGroupRef decls
	/// @return true or false
	bool HandleTopLevelDecl(DeclGroupRef decls) override;
};

class PragmaReaderAction {
private:
	PPContext &pre_processor_context;

public:
	/// Constructor of class FileAnalyser
	/// @param Object PPContext of clang tooling
	/// @return this object
	PragmaReaderAction(PPContext &pre_processor_context);

	/// Analyse Methode of class FileAnalyser
	/// pass the AST (abstract syntax tree) of the sourcefile
	/// @param ASTConsumer * custom_ast_consumer
	/// @return void
	bool run(ASTConsumer *custom_ast_consumer);

};


#endif //CORE_PRAGMAREADERACTION_H
