/**
 * @author Sirko Höer
 * @author Lennart Haas (parts marked)
 */


#include <clang/AST/AST.h>
#include <clang/Parse/ParseAST.h>

#include <iostream>
#include <clang/AST/ASTConsumer.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Rewrite/Frontend/Rewriters.h>

#include "PragmaReaderAction.h"

#include "../preprocessor/PPContext.h"

using namespace std;
using namespace clang;
using namespace clang::tooling;
using namespace llvm;


PragmaReaderAction::PragmaReaderAction(PPContext &pre_processor_context) : pre_processor_context(
		pre_processor_context) {}

/// Constructor of class FileAnalyser
/// pass the AST (abstract syntax tree) of the sourcefile
/// @param ASTConsumer * custom_ast_consumer
/// @return void
bool PragmaReaderAction::run(ASTConsumer *custom_ast_consumer) {

	std::vector<string> source_file = pre_processor_context.getSourceFile();
	auto &compiler_instance = pre_processor_context.getCompilerInstance();

	compiler_instance.setASTConsumer(llvm::make_unique<ASTConsumer>(*custom_ast_consumer));

	for (const auto &item : source_file) {
		pre_processor_context.setMainFileToParse(item);
		compiler_instance.getDiagnosticClient().BeginSourceFile(compiler_instance.getLangOpts(),
																&compiler_instance.getPreprocessor());

		ParseAST(compiler_instance.getPreprocessor(), custom_ast_consumer, compiler_instance.getASTContext());
		break;
	}
	return true;
}

/// HandleTopLevelDecl is entrypoint to start traversing through the AST
/// @param DeclGroupRef decls
/// @return true or false
bool PragmaReaderASTConsumer::HandleTopLevelDecl(DeclGroupRef decls) {

	for (auto &decl : decls) {
		Visitor.TraverseDecl(decl);
	}
	return true;
}

bool PragmaReaderVisitor::VisitStmt(Stmt *s) {

	return true;
}


bool PragmaReaderVisitor::VisitFunctionDecl(FunctionDecl *f) {
	if (clang::isa<clang::FunctionDecl>(f)) {
		pragmaHandler->addFunc(f); // Line added by Lennart Haas
	}
	return true;
}

bool PragmaReaderVisitor::VisitVarDecl(VarDecl *v) {
	return true;
}
