/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrSanitizers::FuzzPragmaAttrSanitizers(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "l_paren"))
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}
	parse_sanitizers(Tok);
}

void FuzzPragmaAttrSanitizers::dump(std::string line_prefix)
{
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "use AddressSanitizer " << (address ? "TRUE" : "FALSE") << "\n";
	llvm::outs() << line_prefix << "use LeakSanitizer " << (leak ? "TRUE" : "FALSE") << "\n";
}

std::string FuzzPragmaAttrSanitizers::getAttrType()
{
	return "Sanitizers";
}

CallPos FuzzPragmaAttrSanitizers::getPosition()
{
	return CallPos::NoPos;
}

void FuzzPragmaAttrSanitizers::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl *> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{}

void FuzzPragmaAttrSanitizers::parse_sanitizers(clang::Token &Tok)
{
	std::vector<std::string> names;
	PP.Lex(Tok);
	while(strcmp(Tok.getName(), "r_paren") != 0)
	{
		if(strcmp(Tok.getName(), "identifier") == 0)
		{
			names.push_back(Tok.getIdentifierInfo()->getName().str());
		}
		else if(strcmp(Tok.getName(), "comma") != 0)
		{
			clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Sanitizer Option should only be a comma-separated list of sanitizer names"));
		}
		PP.Lex(Tok);
	}
	address = false;
	leak = false;
	for(const auto name : names)
	{
		if(name == "address")
		{
			address = true;
		}
		if(name == "leak")
		{
			leak = true;
		}
	}
}