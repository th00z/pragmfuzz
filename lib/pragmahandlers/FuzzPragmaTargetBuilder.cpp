/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaHandler.h"

FuzzPragmaAttribute *FuzzPragmaTargetBuilder::getParam(std::string type, std::unique_ptr<pragmaInfo> &pragma)
{
	for(auto elem : pragma->pragmaParams)
	{
		if(elem->getAttrType() == type)
		{
			return elem;
		}
	}
	return (FuzzPragmaAttribute*) nullptr;
}

FuzzPragmaAttribute *FuzzPragmaTargetBuilder::getParam(CallPos pos, std::unique_ptr<pragmaInfo> &pragma)
{
	for(auto elem : pragma->pragmaParams)
	{
		if(elem->getPosition() == pos)
		{
			return elem;
		}
	}
	return (FuzzPragmaAttribute*) nullptr;
}

std::vector<FuzzPragmaAttribute*> FuzzPragmaTargetBuilder::getParams(CallPos pos, std::unique_ptr<pragmaInfo> &pragma)
{
	std::vector<FuzzPragmaAttribute*> vec;
	for(auto elem : pragma->pragmaParams)
	{
		if(elem->getPosition() == pos)
		{
			vec.push_back(elem);
		}
	}
	return vec;
}

std::vector<FuzzPragmaAttribute*> FuzzPragmaTargetBuilder::getParams(std::string type, std::unique_ptr<pragmaInfo> &pragma)
{
	std::vector<FuzzPragmaAttribute*> vec;
	for(auto elem : pragma->pragmaParams)
	{
		if(elem->getAttrType() == type)
		{
			vec.push_back(elem);
		}
	}
	return vec;
}

void FuzzPragmaTargetBuilder::buildCall(clang::ASTContext &context, std::vector<clang::Stmt *> &stmts, clang::FunctionDecl *targetfunc, clang::ParmVarDecl *buf, clang::ParmVarDecl *size)
{
	std::vector<clang::Expr *> ArgInfo;
	ArgInfo.push_back(clang::ImplicitCastExpr::Create
							  (
									  context,
									  buf->getType().getNonReferenceType(),
									  clang::CK_LValueToRValue,
									  clang::DeclRefExpr::Create
											  (
													  context,
													  clang::NestedNameSpecifierLoc(),
													  clang::SourceLocation(),
													  buf,
													  false,
													  clang::SourceLocation(),
													  buf->getType().getNonReferenceType(),
													  clang::VK_LValue
											  ),
									  nullptr,
									  clang::VK_RValue
							  ));
	ArgInfo.push_back(clang::ImplicitCastExpr::Create
							  (
									  context,
									  size->getType().getNonReferenceType(),
									  clang::CK_LValueToRValue,
									  clang::DeclRefExpr::Create
											  (
													  context,
													  clang::NestedNameSpecifierLoc(),
													  clang::SourceLocation(),
													  size,
													  false,
													  clang::SourceLocation(),
													  size->getType().getNonReferenceType(),
													  clang::VK_LValue
											  ),
									  nullptr,
									  clang::VK_RValue
							  ));
	clang::ImplicitCastExpr *funcExpr = clang::ImplicitCastExpr::Create
			(
					context,
					targetfunc->getType().getNonReferenceType(),
					clang::CK_FunctionToPointerDecay,
					clang::DeclRefExpr::Create
							(
									context,
									clang::NestedNameSpecifierLoc(),
									clang::SourceLocation(),
									targetfunc,
									false,
									clang::SourceLocation(),
									targetfunc->getType().getNonReferenceType(),
									clang::VK_LValue
							),
					nullptr,
					clang::VK_LValue
			);
	stmts.push_back(new(context) clang::CallExpr(context, funcExpr, llvm::ArrayRef<clang::Expr *>(ArgInfo), targetfunc->getReturnType().getNonReferenceType(), clang::VK_LValue, clang::SourceLocation()));
}

void FuzzPragmaTargetBuilder::createTargetFunc(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, clang::ASTContext &context, clang::TranslationUnitDecl *TopDecl, clang::IdentifierTable &identifiers, const std::vector<clang::FunctionDecl *> &functionDecls)
{

	std::vector<clang::QualType> ArgsTy;
	ArgsTy.push_back(context.IntTy);
	ArgsTy.push_back(context.IntTy);

	clang::QualType functype = context.getFunctionType(context.IntTy, llvm::ArrayRef<clang::QualType>(ArgsTy), clang::FunctionProtoType::ExtProtoInfo());

	clang::FunctionDecl *llvmFuzzerTestOneInput = clang::FunctionDecl::Create
			(
					context,
					TopDecl,
					clang::SourceLocation(),
					clang::SourceLocation(),
					clang::DeclarationName(&identifiers.get("LLVMFuzzerTestOneInput")),
					functype,
					nullptr,
					clang::SC_Extern,
					false
			);

	std::vector<clang::ParmVarDecl*> NewParamInfo;

	clang::ParmVarDecl *buf = clang::ParmVarDecl::Create
			(
					context,
					llvmFuzzerTestOneInput,
					clang::SourceLocation(),
					clang::SourceLocation(),
					&identifiers.get("buf"),
					context.IntTy,
					nullptr,
					clang::SC_None,
					nullptr
			);

	clang::ParmVarDecl *size = clang::ParmVarDecl::Create
			(
					context,
					llvmFuzzerTestOneInput,
					clang::SourceLocation(),
					clang::SourceLocation(),
					&identifiers.get("size"),
					context.IntTy,
					NULL,
					clang::SC_None,
					NULL
			);

	NewParamInfo.push_back(buf);
	NewParamInfo.push_back(size);

	llvmFuzzerTestOneInput->setParams(llvm::ArrayRef<clang::ParmVarDecl*>(NewParamInfo));


	std::vector<clang::Stmt*> stmts;
	std::map<std::string, clang::ParmVarDecl*> vars;

	vars.insert(std::pair<std::string, clang::ParmVarDecl*>("buf", buf));
	vars.insert(std::pair<std::string, clang::ParmVarDecl*>("size", size));

	std::vector<FuzzPragmaAttribute*> attrbefore = getParams(CallPos::Before, pragma);
	std::vector<FuzzPragmaAttribute*> attrreplace= getParams(CallPos::Replace, pragma);
	std::vector<FuzzPragmaAttribute*> attrafter = getParams(CallPos::After, pragma);
	std::vector<FuzzPragmaAttribute*> attrfull = getParams(CallPos::FullBody, pragma);
	std::vector<FuzzPragmaAttribute*> attrno = getParams(CallPos::NoPos, pragma);

	if(!attrfull.empty())
	{
		FuzzPragmaAttribute *fullbody = attrfull.back();
		attrfull.pop_back();
		if(!attrfull.empty())
		{
			// TODO: Throw error for multiple fullbody parameters
		}
		fullbody->fillVectorCallback(context, llvmFuzzerTestOneInput, identifiers, stmts, vars, functionDecls);
	}
	else
	{
		if(!attrbefore.empty())
		{
			for(auto elem : attrbefore)
			{
				elem->fillVectorCallback(context, llvmFuzzerTestOneInput, identifiers, stmts, vars, functionDecls);
			}
		}
		if(!attrreplace.empty())
		{
			for(auto elem : attrreplace)
			{
				elem->fillVectorCallback(context, llvmFuzzerTestOneInput, identifiers, stmts, vars, functionDecls);
			}
		}
		else
		{
			FuzzPragmaAttrCall *callparam = (FuzzPragmaAttrCall*) getParam("Call", pragma);
			if(callparam)
			{
				callparam->fillVectorCallback(context, targetfunc, identifiers, stmts, vars, functionDecls);
			}
			else
			{
				buildCall(context, stmts, targetfunc, buf, size);
			}
		}

		bool inlineafter = false;
		if(!attrafter.empty())
		{
			for(auto elem : attrafter)
			{
				elem->fillVectorCallback(context, llvmFuzzerTestOneInput, identifiers, stmts, vars, functionDecls);
				if(elem->getAttrType() == "Inline")
				{
					inlineafter = true;
				}
			}
		}
		if(!inlineafter)
		{
			stmts.push_back(new(context) clang::ReturnStmt(clang::SourceLocation(), clang::IntegerLiteral::Create(context, llvm::APInt(1, 0, false), context.IntTy, clang::SourceLocation()), NULL));
		}
	}
	// http://www.tierchenwelt.de/images/stories/fotos/saeugetiere/kloakentiere/schnabeltier/schnabeltier_steckbrief_l.jpg

	llvmFuzzerTestOneInput->setBody
			(
					clang::CompoundStmt::Create
							(
									context,
									llvm::ArrayRef<clang::Stmt*>(stmts),
									clang::SourceLocation(),
									clang::SourceLocation()
							)
			);
	TopDecl->addDecl(llvmFuzzerTestOneInput);
}

std::string FuzzPragmaTargetBuilder::getSourceAsString(clang::TranslationUnitDecl *TopDecl)
{
	std::string out;
	llvm::raw_string_ostream sstream(out);
	TopDecl->print(sstream);
	return out;
}

void FuzzPragmaTargetBuilder::str_replace(std::string &str, const std::string &from, const std::string &to)
{
	size_t start_pos = 0;
	while((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
}

void FuzzPragmaTargetBuilder::dirtyHack(std::string &source, std::vector<FuzzPragmaAttribute*> includes)
{
	str_replace(source, "extern int LLVMFuzzerTestOneInput(int buf, int size)", "extern \"C\" int LLVMFuzzerTestOneInput(uint8_t *buf, size_t size)");
	if(!includes.empty())
	{
		std::string str = std::string("");
		for(auto elem : includes)
		{
			((FuzzPragmaAttrInclheader*) elem)->getInclude(str);
		};
		str.append(std::string("\n") + source);
		source = str;
	}
	else
	{
		source = std::string("\n") + source;
	}
	source = std::string("#include <stdint.h>\n#include <stddef.h>\n") + source;
}

std::string FuzzPragmaTargetBuilder::output()
{
	return out.str();
}

void FuzzPragmaTargetBuilder::buildTarget(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	std::unique_ptr<clang::CompilerInstance> compiler_instance(new clang::CompilerInstance());
	compiler_instance->createDiagnostics();
	std::shared_ptr<clang::TargetOptions> target_options(new clang::TargetOptions());
	target_options->Triple = llvm::sys::getProcessTriple();
	compiler_instance->setTarget(clang::TargetInfo::CreateTargetInfo(compiler_instance->getDiagnostics(), target_options));
	compiler_instance->createFileManager();
	compiler_instance->createSourceManager(compiler_instance->getFileManager());
	compiler_instance->createPreprocessor(clang::TU_Complete);
	compiler_instance->createASTContext();
	clang::ASTContext &context = compiler_instance->getASTContext();
	clang::IdentifierTable &identifiers = compiler_instance->getPreprocessor().getIdentifierTable();

	std::vector<FuzzPragmaAttribute*> includes = getParams("Inclheader", pragma);

	clang::TranslationUnitDecl *TopDecl = context.getTranslationUnitDecl();

	createTargetFunc(std::move(pragma), targetfunc, context, TopDecl, identifiers, functionDecls);

	std::string source = getSourceAsString(TopDecl);

	dirtyHack(source, includes);

	out << source << "\n";

	// TODO: Escaping für Targetname (Funktionsname kann chars enthalten die im Dateisystem nicht erlaubt sind)
	std::error_code error_code;
	llvm::raw_fd_ostream outFile(targetFile, error_code, llvm::sys::fs::F_None);
	outFile << source;
	outFile.close();
}

FuzzPragmaTargetBuilder::FuzzPragmaTargetBuilder(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	buildTarget(std::move(pragma), targetfunc, std::move(targetFile), functionDecls);
}

std::string FuzzPragmaTargetBuilder::Run(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	return (new FuzzPragmaTargetBuilder(std::move(pragma), targetfunc, std::move(targetFile), functionDecls))->output();
}