/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#ifndef PRAGMFUZZ_PRAGMAHANDLER
#define PRAGMFUZZ_PRAGMAHANDLER

#include <clang/Rewrite/Core/Rewriter.h>
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Token.h"
#include "clang/Basic/IdentifierTable.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/AST/Decl.h"
#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <iostream>
#include <climits>
#include <sstream>
#include <stdint.h>
#include <stddef.h>
#include <future>
#include <boost/algorithm/string.hpp>
#include "FuzzPragmaAttributes.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/DiagnosticRenderer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Basic/Specifiers.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Stmt.h"

struct pragmaInfo
{
	clang::SourceLocation location;
	std::vector<FuzzPragmaAttribute*> pragmaParams;
};

class FuzzPragmaHandler : public clang::PragmaHandler {

	private:

		// Preprocessor for lexing the pragmas
		clang::Preprocessor &PP;

		// DiagnosticsEngine for error reporting
		clang::DiagnosticsEngine &DE;

		// Map that contains all possible error kinds that can happen in this class
		std::map<std::string, const unsigned> errors;

		// Map with information forged from all the fuzz pragmas
		std::vector<std::unique_ptr<pragmaInfo>> pragmaData;

		std::vector<clang::FunctionDecl*> functionDecls;

		std::vector<FuzzPragmaAttribute*> ParsePragma(clang::Token &Tok);

		bool checkSameFile(clang::SourceLocation funcLoc, clang::SourceLocation pragmaLoc);

	public:

		FuzzPragmaHandler(clang::Preprocessor &PP);

		void HandlePragma(clang::Preprocessor &PP, clang::PragmaIntroducerKind Introducer, clang::Token &firstToken) override;

		void addFunc(clang::FunctionDecl *f);

		void process();

		clang::FunctionDecl *findFunction(clang::SourceLocation pragmaLoc);

		void dump();

};

class FuzzPragmaTargetBuilder
{
	private:

		std::stringstream out;

		void str_replace(std::string &str, const std::string &from, const std::string &to);

		void dirtyHack(std::string &source, std::vector<FuzzPragmaAttribute*> includes);

		std::string getSourceAsString(clang::TranslationUnitDecl *TopDecl);

		void createTargetFunc(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, clang::ASTContext &context, clang::TranslationUnitDecl *TopDecl, clang::IdentifierTable &identifiers, const std::vector<clang::FunctionDecl*> &functionDecls);

		FuzzPragmaAttribute *getParam(std::string type, std::unique_ptr<pragmaInfo> &pragma);

		FuzzPragmaAttribute *getParam(CallPos pos, std::unique_ptr<pragmaInfo> &pragma);

		std::vector<FuzzPragmaAttribute*> getParams(CallPos pos, std::unique_ptr<pragmaInfo> &pragma);

		std::vector<FuzzPragmaAttribute*> getParams(std::string type, std::unique_ptr<pragmaInfo> &pragma);

		void buildCall(clang::ASTContext &context, std::vector<clang::Stmt *> &stmts, clang::FunctionDecl *targetfunc, clang::ParmVarDecl *buf, clang::ParmVarDecl *size);

		void buildTarget(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl*> &functionDecls);

	public:

		FuzzPragmaTargetBuilder(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl*> &functionDecls);

		static std::string Run(std::unique_ptr<pragmaInfo> pragma, clang::FunctionDecl *targetfunc, const std::string targetFile, const std::vector<clang::FunctionDecl*> &functionDecls);

		std::string output();
};

#endif //PRAGMFUZZ_PRAGMAHANDLER
