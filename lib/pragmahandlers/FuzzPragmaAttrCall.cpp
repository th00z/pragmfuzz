/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrCall::FuzzPragmaAttrCall(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	clang::SourceManager &SM = PP.getSourceManager();

	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "l_paren") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}

	PP.Lex(Tok);
	while(strcmp(Tok.getName(), "r_paren") != 0)
	{
		if(strcmp(Tok.getName(), "comma") != 0)
		{
			paramInfo.push_back(buildParamInfo(SM, Tok, PP));
		}
		PP.Lex(Tok);
	}
}

void FuzzPragmaAttrCall::dump(std::string line_prefix)
{
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "Args: ";
	for(const auto &elem : paramInfo)
	{
		llvm::outs() << elem->val << ", ";
	}
	llvm::outs() << "\n";
}

std::string FuzzPragmaAttrCall::getAttrType()
{
	return "Call";
}

CallPos FuzzPragmaAttrCall::getPosition()
{
	return CallPos::NoPos;
}

void FuzzPragmaAttrCall::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl *> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	std::vector<clang::Expr *> ArgInfo = buildArgVec(std::move(paramInfo), context, decls, functionDecls, DE);

	clang::ImplicitCastExpr *funcExpr = clang::ImplicitCastExpr::Create
	(
		context,
		func->getType().getNonReferenceType(),
		clang::CK_FunctionToPointerDecay,
		clang::DeclRefExpr::Create
		(
			context,
			clang::NestedNameSpecifierLoc(),
			clang::SourceLocation(),
			func,
			false,
			clang::SourceLocation(),
			func->getType().getNonReferenceType(),
			clang::VK_LValue
		),
		nullptr,
		clang::VK_LValue
	);
	stmts.push_back(new(context) clang::CallExpr(context, funcExpr, llvm::ArrayRef<clang::Expr *>(ArgInfo), func->getReturnType().getNonReferenceType(), clang::VK_LValue, clang::SourceLocation()));
}