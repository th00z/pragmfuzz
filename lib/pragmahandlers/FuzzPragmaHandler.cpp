/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaHandler.h"

FuzzPragmaHandler::FuzzPragmaHandler(clang::Preprocessor &Preprocessor) : PP(Preprocessor), DE(PP.getDiagnostics())
{
	errors.insert
	(
		std::pair<std::string, const unsigned>
		(
			"expected_identifier",
			DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be an identifier!")
		)
	);
	errors.insert
	(
		std::pair<std::string, const unsigned>
		(
			"expected_l_paren",
			DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!")
		)
	);
	errors.insert
	(
		std::pair<std::string, const unsigned>
		(
			"unknown_param",
			DE.getCustomDiagID(clang::DiagnosticsEngine::Warning, "Unknown fuzzing-parameter '%0'.")
		)
	);
	errors.insert
	(
		std::pair<std::string, const unsigned>
		(
			"no_function_after_pragma",
			DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "There is no function to fuzz after the pragma!")
		)
	);
}

void FuzzPragmaHandler::HandlePragma(clang::Preprocessor &PP, clang::PragmaIntroducerKind Introducer, clang::Token &firstToken)
{
	std::unique_ptr<pragmaInfo> thisPragma(new pragmaInfo);
	thisPragma->location= firstToken.getLocation();
	thisPragma->pragmaParams = ParsePragma(firstToken);
	pragmaData.push_back(std::move(thisPragma));
}

std::vector<FuzzPragmaAttribute*> FuzzPragmaHandler::ParsePragma(clang::Token &Tok)
{
	std::vector<FuzzPragmaAttribute*> attributeVec;
	while(strcmp(Tok.getName(), "eod"))
	{
		if(strcmp(Tok.getName(), "identifier"))
		{
			clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), errors["expected_identifier"]);
		}
		else
		{
			auto identifier = Tok.getIdentifierInfo()->getName().str();
			if(identifier == "functioncall")
			{
				attributeVec.push_back(new FuzzPragmaAttrFunctionCall(PP, Tok));
			}
			else if(identifier == "sanitizers")
			{
				attributeVec.push_back(new FuzzPragmaAttrSanitizers(PP, Tok));
			}
			else if(identifier == "corpus")
			{
				attributeVec.push_back(new FuzzPragmaAttrCorpus(PP, Tok));
			}
			else if(identifier == "call")
			{
				attributeVec.push_back(new FuzzPragmaAttrCall(PP, Tok));
			}
			else if(identifier == "inlinefunc")
			{
				attributeVec.push_back(new FuzzPragmaAttrInline(PP, Tok));
			}
			else if(identifier == "inclheader")
			{
				attributeVec.push_back(new FuzzPragmaAttrInclheader(PP, Tok));
			}
			else
			{
				clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), errors["unknown_param"]);
				DB.AddString(identifier);
			}
		}
		PP.Lex(Tok);
	}
	return attributeVec;
}

void FuzzPragmaHandler::addFunc(clang::FunctionDecl *f)
{
	functionDecls.push_back(f);
}

void FuzzPragmaHandler::dump()
{
	llvm::outs() << "FuzzPragmas:" << "\n";
	for(const auto &elem : pragmaData)
	{
		llvm::outs() << "\t" << "FuzzPragma:" << "\n";
		llvm::outs() << "\t\t" << "Location: " << elem->location.printToString(PP.getSourceManager()) << "\n";
		for(const auto &elem2 : elem->pragmaParams)
		{
			llvm::outs() << "\t\t" << "PragmaParam:" << "\n";
			elem2->dump("\t\t\t");
		}
		llvm::outs() << "\n";
	}
}

void FuzzPragmaHandler::process()
{
	int i = 0;
	std::vector<std::future<std::string>> threads;
	while(!pragmaData.empty())
	{
		auto pragma = std::move(pragmaData.back());
		pragmaData.pop_back();
		clang::FunctionDecl *pragmaFunction = findFunction(pragma->location);
		if(pragmaFunction == nullptr)
		{
			clang::DiagnosticBuilder DB = DE.Report(pragma->location, errors["no_function_after_pragma"]);
			return;
		}
		std::stringstream target;
		target << "fuzztarget" << ++i << "_" << pragmaFunction->getName().str() << ".c";
		threads.push_back(async(std::launch::async, FuzzPragmaTargetBuilder::Run, std::move(pragma), pragmaFunction, target.str(), functionDecls));
	}
	for(auto &future : threads)
	{
		llvm::outs() <<  future.get();
	}
}

clang::FunctionDecl *FuzzPragmaHandler::findFunction(clang::SourceLocation pragmaLoc)
{
	int locationDiff = INT_MAX;
	clang::FunctionDecl *curfunc = nullptr;
	for(const auto &f : functionDecls)
	{
		if (!checkSameFile(f->getLocation(), pragmaLoc))
		{
			continue;
		}

		if((f->getLocStart().getRawEncoding() > pragmaLoc.getRawEncoding()) && ((int) f->getLocStart().getRawEncoding() - (int) pragmaLoc.getRawEncoding() < locationDiff))
		{
			locationDiff = f->getLocStart().getRawEncoding() - pragmaLoc.getRawEncoding();
			curfunc = f;
		}
	}
	return curfunc;
}

bool FuzzPragmaHandler::checkSameFile(clang::SourceLocation funcLoc, clang::SourceLocation pragmaLoc)
{
	std::string f = funcLoc.printToString(PP.getSourceManager());
	std::string p = pragmaLoc.printToString(PP.getSourceManager());
	std::vector<std::string> fparts;
	std::vector<std::string> pparts;
	boost::split(fparts, f, [](char c){return c == ':';});
	boost::split(pparts, p, [](char c){return c == ':';});
	if(fparts.empty() || pparts.empty())
	{
		return false;
	}

	return (fparts[0] == pparts[0]);
}