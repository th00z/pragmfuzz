/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrInclheader::FuzzPragmaAttrInclheader(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	PP.Lex(Tok);
	if((strcmp(Tok.getName(), "l_paren")) != 0 && (strcmp(Tok.getName(), "comma") != 0))
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}
	parse_inclstring(Tok);

	if(strcmp(PP.LookAhead(0).getName(), "comma") == 0)
	{
		nextNode = new FuzzPragmaAttrInclheader(PP, Tok);
	}
	else
	{
		PP.Lex(Tok);
		if (strcmp(Tok.getName(), "r_paren") != 0)
		{
			clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: Attribute should be closed with a ')'!"));
		}
	}
}

void FuzzPragmaAttrInclheader::dump(std::string line_prefix)
{
	std::string holder = std::string("");
	getInclude(holder, false);
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "Includestring: " << holder << "\n";

	FuzzPragmaAttrInclheader *curHeader = this;
	while((curHeader = curHeader->nextNode) != nullptr)
	{
		holder = std::string("");
		curHeader->getInclude(holder, false);
		llvm::outs() << line_prefix << "Includestring: " << holder << "\n";
	}
}

std::string FuzzPragmaAttrInclheader::getAttrType()
{
	return "Inclheader";
}

CallPos FuzzPragmaAttrInclheader::getPosition()
{
	return CallPos::NoPos;
}

void FuzzPragmaAttrInclheader::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl *> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{}

void FuzzPragmaAttrInclheader::parse_inclstring(clang::Token &Tok)
{
	clang::SourceManager &SM = PP.getSourceManager();

	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "string_literal") == 0)
	{
		inclstart = '"';
		inclend = '"';

		clang::SourceLocation lStart = Tok.getLocation();
		clang::SourceLocation lEnd = Tok.getEndLoc();
		includefile = std::string(SM.getCharacterData(lStart) + 1, SM.getCharacterData(lEnd) - SM.getCharacterData(lStart) - 2);
	}
	else if(strcmp(Tok.getName(), "less") == 0)
	{
		std::stringstream str;
		inclstart = '<';
		inclend = '>';

		PP.Lex(Tok);
		while(strcmp(Tok.getName(), "greater") != 0)
		{
			clang::SourceLocation lStart =Tok.getLocation();
			clang::SourceLocation lEnd = Tok.getEndLoc();
			str << std::string(SM.getCharacterData(lStart), SM.getCharacterData(lEnd) - SM.getCharacterData(lStart));
			PP.Lex(Tok);
		}
		includefile = str.str();
	}
}

void FuzzPragmaAttrInclheader::getInclude(std::string &str)
{
	getInclude(str, true);
}

void FuzzPragmaAttrInclheader::getInclude(std::string &str, bool recursive)
{
	str.append(std::string("#include ") + inclstart + includefile + inclend);
	if(recursive)
	{
		str.append(std::string("\n"));
	}
	if(nextNode && recursive)
	{
		FuzzPragmaAttrInclheader* curHeader = this;
		while((curHeader = curHeader->nextNode) != nullptr)
		{
			str.append(std::string("#include ") + curHeader->inclstart + curHeader->includefile + curHeader->inclend + std::string("\n"));
		}
	}
}