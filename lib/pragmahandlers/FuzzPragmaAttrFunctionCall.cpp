/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrFunctionCall::FuzzPragmaAttrFunctionCall(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "l_paren") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}

	bool boxed = false;
	if(strcmp(PP.LookAhead(0).getName(), "l_paren") == 0)
	{
		boxed = true;
		PP.Lex(Tok);
	}

	parse_funcdecl(Tok);
	if(strcmp(PP.LookAhead(0).getName(), "comma") == 0)
	{
		PP.Lex(Tok);
		parse_position(Tok);
	}
	if(strcmp(PP.LookAhead(0).getName(), "comma") == 0)
	{
		PP.Lex(Tok);
		PP.Lex(Tok);
		parse_vardecl(Tok);
	}

	if(boxed)
	{
		PP.Lex(Tok);
		if(strcmp(Tok.getName(), "r_paren") != 0)
		{
			clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Functioncall Syntax: Boxed functions should be closed with a ')'!"));
		}
	}
	if(boxed && (strcmp(PP.LookAhead(0).getName(), "comma") == 0))
	{
		nextNode = new FuzzPragmaAttrFunctionCall(PP, Tok);
	}
	else
	{
		PP.Lex(Tok);
		if (strcmp(Tok.getName(), "r_paren") != 0)
		{
			clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: FunctionCall expects 1 or 2 arguments and should be closed by a right parenthesis afterwards!"));
		}
	}
}

void FuzzPragmaAttrFunctionCall::dump(std::string line_prefix)
{
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "Func-Name: " << fname << "\n";
	llvm::outs() << line_prefix << "Args: ";
	for(const auto &elem : paramInfo)
	{
		llvm::outs() << elem->val << ", ";
	}
	llvm::outs() << "\n" << line_prefix << "Returnvar: " << prepvarname << "\n";

	FuzzPragmaAttrFunctionCall *curNode = this;
	while((curNode = curNode->nextNode) != nullptr)
	{
		llvm::outs() << line_prefix << "Func-Name: " << curNode->fname << "\n";
		llvm::outs() << line_prefix << "Args: ";
		for(const auto &elem : curNode->paramInfo)
		{
			llvm::outs() << elem->val << ", ";
		}
		llvm::outs() << "\n" << line_prefix << "Returnvar: " << curNode->prepvarname << "\n";
	}
}

std::string FuzzPragmaAttrFunctionCall::getAttrType()
{
	return "FunctionCall";
}

CallPos FuzzPragmaAttrFunctionCall::getPosition()
{
	return position;
}

void FuzzPragmaAttrFunctionCall::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl *> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	stmts.push_back(buildPrepFunctionCall(context, identifiers, decls, functionDecls));
	auto prepdecl = getPrepareVar();
	if(prepdecl)
	{
		decls.insert(std::pair<std::string, clang::ParmVarDecl*>
		(
			 prepdecl->getName(),
			 clang::ParmVarDecl::Create
			 (
				 context,
				 func,
				 clang::SourceLocation(),
				 clang::SourceLocation(),
				 &identifiers.get(prepdecl->getName()),
				 prepdecl->getType(),
				 NULL,
				 prepdecl->getStorageClass(),
				 NULL
			 )
		));
	}
}

void FuzzPragmaAttrFunctionCall::parse_funcdecl(clang::Token &Tok)
{
	clang::SourceManager &SM = PP.getSourceManager();

	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "identifier") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "FunctionCall Option expects a function call, no function name found here!"));
	}
	fname = Tok.getIdentifierInfo()->getName().str();
	fnameFoundAt = Tok.getLocation();

	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "l_paren") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Error: Left parenthesis expected! FunctionCall expects a complete function call as first argument!"));
	}

	PP.Lex(Tok);
	while(strcmp(Tok.getName(), "r_paren") != 0)
	{
		if(strcmp(Tok.getName(), "comma") != 0)
		{
			paramInfo.push_back(buildParamInfo(SM, Tok, PP));
		}
		PP.Lex(Tok);
	}
}

void FuzzPragmaAttrFunctionCall::parse_vardecl(clang::Token &Tok)
{
	if(strcmp(Tok.getName(), "identifier") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "New variable name expected, '%0' given!"));
		DB.AddString(Tok.getName());
	}
	else
	{
		prepvarname = Tok.getIdentifierInfo()->getName().str();
	}
}

void FuzzPragmaAttrFunctionCall::parse_position(clang::Token &Tok)
{
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "identifier") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Inline Option requires a third Parameter!"));
	}
	auto posval = Tok.getIdentifierInfo()->getName().str();
	if(posval == "before")
	{
		position = CallPos::Before;
	}
	else if(posval == "after")
	{
		position = CallPos::After;
	}
	else
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Third argument of FunctionCall Option should be either before or after!"));
	}
}

clang::VarDecl* FuzzPragmaAttrFunctionCall::getPrepareVar()
{
	return preparevar;
}

clang::Stmt* FuzzPragmaAttrFunctionCall::buildPrepFunctionCall(clang::ASTContext &context, clang::IdentifierTable &identifiers, clang::ParmVarDecl *buf, clang::ParmVarDecl *size, std::map<std::string, clang::ParmVarDecl *> vars, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	vars.insert(std::pair<std::string, clang::ParmVarDecl*>("buf", buf));
	vars.insert(std::pair<std::string, clang::ParmVarDecl*>("size", size));
	return buildPrepFunctionCall(context, identifiers, vars, functionDecls);
}

clang::Stmt* FuzzPragmaAttrFunctionCall::buildPrepFunctionCall(clang::ASTContext &context, clang::IdentifierTable &identifiers, std::map<std::string, clang::ParmVarDecl *> vars, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	clang::TranslationUnitDecl *TopDecl = context.getTranslationUnitDecl();

	clang::FunctionDecl *target = findFunction(fname, functionDecls, false);
	if(!target)
	{
		clang::DiagnosticBuilder DB = DE.Report(fnameFoundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Function that should be called was not found!"));
		return (clang::Stmt*) nullptr;
	}

	auto funccall = new (context) clang::CallExpr
			(
					context,
					clang::ImplicitCastExpr::Create
							(
									context,
									target->getReturnType().getNonReferenceType(),
									clang::CK_LValueToRValue,
									clang::ImplicitCastExpr::Create
											(
													context,
													target->getReturnType().getNonReferenceType(),
													clang::CK_FunctionToPointerDecay,
													clang::DeclRefExpr::Create
															(
																	context,
																	clang::NestedNameSpecifierLoc(),
																	clang::SourceLocation(),
																	target,
																	false,
																	clang::SourceLocation(),
																	target->getReturnType().getNonReferenceType(),
																	clang::VK_LValue
															),
													nullptr,
													clang::VK_LValue
											),
									nullptr,
									clang::VK_RValue
							),
					llvm::ArrayRef<clang::Expr *>(buildArgVec(std::move(paramInfo), context, vars, functionDecls, DE)),
					target->getReturnType().getNonReferenceType(),
					clang::VK_LValue,
					clang::SourceLocation()
			);

	auto type = target->getReturnType();

	if ((type.getAsString() != "void") && (prepvarname != ""))
	{
		preparevar = clang::VarDecl::Create
				(
						context,
						TopDecl,
						clang::SourceLocation(),
						clang::SourceLocation(),
						&identifiers.get(prepvarname),
						type,
						NULL,
						target->getStorageClass()
				);
		preparevar->setInit(funccall);
		return new(context) clang::DeclStmt(clang::DeclGroupRef(preparevar), clang::SourceLocation(), clang::SourceLocation());
	}
	else
	{
		if(prepvarname != "")
		{
			clang::DiagnosticBuilder DB = DE.Report(fnameFoundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Warning, "Returnvar attribute given but function is of type void! Not using variable name."));
		}
		return funccall;
	}
}