/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrCorpus::FuzzPragmaAttrCorpus(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "l_paren") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}
	corpus_path = "";
	PP.Lex(Tok);
	while(strcmp(Tok.getName(), "r_paren") != 0)
	{
		if(!strcmp(Tok.getName(), "identifier"))
		{
			corpus_path += Tok.getIdentifierInfo()->getName().str();
		}/**/
		else if(!strcmp(Tok.getName(), "slash"))
		{
			corpus_path += "/";
		}/**/
		PP.Lex(Tok);
	}
}

void FuzzPragmaAttrCorpus::dump(std::string line_prefix)
{
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "Path: " << corpus_path << "\n";
}

std::string FuzzPragmaAttrCorpus::getAttrType()
{
	return "Corpus";
}

CallPos FuzzPragmaAttrCorpus::getPosition()
{
	return CallPos::NoPos;
}

void FuzzPragmaAttrCorpus::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl *> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{}