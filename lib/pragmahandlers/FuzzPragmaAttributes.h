/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#ifndef PRAGMFUZZ_FUZZPRAGMAATTRIBUTES_H
#define PRAGMFUZZ_FUZZPRAGMAATTRIBUTES_H

#include <clang/Rewrite/Core/Rewriter.h>
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Token.h"
#include "clang/Basic/IdentifierTable.h"
#include "clang/Lex/Preprocessor.h"
#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <iostream>
#include <climits>
#include <sstream>
#include <stdint.h>
#include <stddef.h>
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/DiagnosticRenderer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Basic/Specifiers.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Expr.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/Stmt.h"

enum class CallPos {Before, Replace, After, FullBody, NoPos};

struct CallTypeInfo
{
	std::string type;
	std::string val;
	clang::SourceLocation foundAt;
};

class FuzzPragmaAttribute
{
	protected:
		std::unique_ptr<CallTypeInfo> buildParamInfo(clang::SourceManager &SM, clang::Token &Tok, clang::Preprocessor &PP);
		std::vector<clang::Expr*> buildArgVec(std::vector<std::unique_ptr<CallTypeInfo>> paramInfo, clang::ASTContext &context, std::map<std::string, clang::ParmVarDecl *> vars, const std::vector<clang::FunctionDecl *> &functionDecls, clang::DiagnosticsEngine &DE);
		void str_replace(std::string &str, const std::string &from, const std::string &to);
		clang::FunctionDecl *findFunction(std::string fname, const std::vector<clang::FunctionDecl *> &functionDecls, bool withBody);

	public:
		virtual void dump(std::string line_prefix) = 0;
		virtual std::string getAttrType() = 0;
		virtual CallPos getPosition() = 0;
		virtual void  fillVectorCallback(clang:: ASTContext &context, clang::FunctionDecl *func, clang:: IdentifierTable &identifiers, std::vector <clang::Stmt *> &stmts, std::map <std::string, clang ::ParmVarDecl*> &decls, const std::vector <clang::FunctionDecl *> &functionDecls) = 0;
};

class FuzzPragmaAttrCall : public FuzzPragmaAttribute
{
private:
	clang::Preprocessor &PP;
	clang::DiagnosticsEngine &DE;
	std::vector<std::unique_ptr<CallTypeInfo>> paramInfo;

public:
	FuzzPragmaAttrCall(clang::Preprocessor &PP, clang::Token &Tok);
	void dump(std::string line_prefix) override;
	std::string getAttrType() override;
	CallPos getPosition() override;
	void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;
};

class FuzzPragmaAttrCorpus : public FuzzPragmaAttribute
{
private:
	clang::Preprocessor &PP;
	clang::DiagnosticsEngine &DE;
	std::string corpus_path;

	// Still to be implemented
	FuzzPragmaAttrCorpus *nextNode;

public:
	FuzzPragmaAttrCorpus(clang::Preprocessor &PP, clang::Token &Tok);
	void dump(std::string line_prefix) override;
	std::string getAttrType() override;
	CallPos getPosition() override;
	void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;
};

class FuzzPragmaAttrFunctionCall : public FuzzPragmaAttribute
{
	private:
		clang::Preprocessor &PP;
		clang::DiagnosticsEngine &DE;
		std::string fname;
		std::string prepvarname = "";
		clang::SourceLocation fnameFoundAt;
		std::vector<std::unique_ptr<CallTypeInfo>> paramInfo;
		clang::VarDecl *preparevar = nullptr;
		CallPos position = CallPos::Before;

		FuzzPragmaAttrFunctionCall *nextNode = nullptr;

		void parse_vardecl(clang::Token &Tok);
		void parse_funcdecl(clang::Token &Tok);
		void parse_position(clang::Token &Tok);

	public:
		FuzzPragmaAttrFunctionCall(clang::Preprocessor &PP, clang::Token &Tok);
		void dump(std::string line_prefix) override;
		std::string getAttrType() override;
		CallPos getPosition() override;
		void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;

		clang::Stmt *buildPrepFunctionCall(clang::ASTContext &context, clang::IdentifierTable &identifiers, clang::ParmVarDecl *buf, clang::ParmVarDecl *size, std::map<std::string, clang::ParmVarDecl*> vars, const std::vector<clang::FunctionDecl *> &functionDecls);
		clang::Stmt *buildPrepFunctionCall(clang::ASTContext &context, clang::IdentifierTable &identifiers, std::map<std::string, clang::ParmVarDecl*> vars, const std::vector<clang::FunctionDecl *> &functionDecls);
		clang::VarDecl *getPrepareVar();
};

class FuzzPragmaAttrInclheader : public FuzzPragmaAttribute
{
private:
	clang::Preprocessor &PP;
	clang::DiagnosticsEngine &DE;

	FuzzPragmaAttrInclheader* nextNode = nullptr;

	std::string includefile;
	char inclstart, inclend;

	void parse_inclstring(clang::Token &Tok);

public:
	FuzzPragmaAttrInclheader(clang::Preprocessor &PP, clang::Token &Tok);
	void dump(std::string line_prefix) override;
	std::string getAttrType() override;
	CallPos getPosition() override;
	void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;

	void getInclude(std::string &str);
	void getInclude(std::string &str, bool recursive);
};

class FuzzPragmaAttrInline : public FuzzPragmaAttribute
{
	private:
		clang::Preprocessor &PP;
		clang::DiagnosticsEngine &DE;
		std::string fname;
		clang::SourceLocation fnameFoundAt;
		std::vector<clang::VarDecl*> funcvars;
		CallPos position = CallPos::Before;

		FuzzPragmaAttrInline *nextNode = nullptr;

		void parse_funcdecl(clang::Token &Tok);
		void parse_position(clang::Token &Tok);
		void fillInlineStmts(const std::vector<clang::FunctionDecl*> &functionDecls, std::vector<clang::Stmt*> &stmts);

	public:
		FuzzPragmaAttrInline(clang::Preprocessor &PP, clang::Token &Tok);
		void dump(std::string line_prefix) override;
		std::string getAttrType() override;
		CallPos getPosition() override;
		void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;

		std::vector<clang::Stmt*> getInlineStmts(const std::vector<clang::FunctionDecl *> &functionDecls);
		std::vector<clang::VarDecl*> getFuncVars();
};

class FuzzPragmaAttrSanitizers : public FuzzPragmaAttribute
{
	private:
		clang::Preprocessor &PP;
		clang::DiagnosticsEngine &DE;
		bool address, leak;

		void parse_sanitizers(clang::Token &Tok);

	public:
		FuzzPragmaAttrSanitizers(clang::Preprocessor &PP, clang::Token &Tok);
		void dump(std::string line_prefix) override;
		std::string getAttrType() override;
		CallPos getPosition() override;
		void fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls) override;
};


#endif //PRAGMFUZZ_FUZZPRAGMAATTRIBUTES_H
