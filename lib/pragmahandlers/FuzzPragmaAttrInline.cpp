/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

FuzzPragmaAttrInline::FuzzPragmaAttrInline(clang::Preprocessor &PP, clang::Token &Tok) : PP(PP), DE(PP.getDiagnostics())
{
	PP.Lex(Tok);
	if((strcmp(Tok.getName(), "l_paren")) != 0 && (strcmp(Tok.getName(), "comma") != 0))
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: There should be a parenthesis after option identifier!"));
	}
	bool boxed = false;

	if((strcmp(PP.LookAhead(0).getName(), "l_paren") == 0))
	{
		boxed = true;
		PP.Lex(Tok);
	}
	parse_funcdecl(Tok);
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "comma") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Inlinefunc Syntax: There should be a comma after function name!"));
	}
	else
	{
		parse_position(Tok);
		if(boxed)
		{
			PP.Lex(Tok);
			if(strcmp(Tok.getName(), "r_paren") != 0)
			{
				clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Inlinefunc Syntax: Boxed inlines should be closed with a ')'!"));
			}
		}
		if(boxed && (strcmp(PP.LookAhead(0).getName(), "comma") == 0))
		{
			nextNode = new FuzzPragmaAttrInline(PP, Tok);
		}
		else
		{
			PP.Lex(Tok);
			if (strcmp(Tok.getName(), "r_paren") != 0)
			{
				clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "You have an error in your Fuzz-Pragma Syntax: Attribute should be closed with a ')'!"));
			}
		}
	}
}

void FuzzPragmaAttrInline::dump(std::string line_prefix)
{
	llvm::outs() << line_prefix << "Name: " << getAttrType() << "\n";
	llvm::outs() << line_prefix << "Inline-Func-Name: " << fname << "\n";

	FuzzPragmaAttrInline *curNode = this;
	while((curNode = curNode->nextNode) != nullptr)
	{
		llvm::outs() << line_prefix << "Inline-Func-Name: " << curNode->fname << "\n";
	}
}

std::string FuzzPragmaAttrInline::getAttrType()
{

	return "Inline";
}

CallPos FuzzPragmaAttrInline::getPosition()
{
	return position;
}

void FuzzPragmaAttrInline::fillVectorCallback(clang::ASTContext &context, clang::FunctionDecl *func, clang::IdentifierTable &identifiers, std::vector<clang::Stmt *> &stmts, std::map<std::string, clang::ParmVarDecl*> &decls, const std::vector<clang::FunctionDecl *> &functionDecls)
{
	for(auto stmt : getInlineStmts(functionDecls))
	{
		stmts.push_back(stmt);
	}
	for(auto decl : funcvars)
	{
		decls.insert(std::pair<std::string, clang::ParmVarDecl*>
							 (
									 decl->getName(),
									 clang::ParmVarDecl::Create
											 (
													 context,
													 func,
													 clang::SourceLocation(),
													 clang::SourceLocation(),
													 &identifiers.get(decl->getName()),
													 decl->getType(),
													 NULL,
													 decl->getStorageClass(),
													 NULL
											 )
							 ));
	}
}

void FuzzPragmaAttrInline::parse_funcdecl(clang::Token &Tok)
{
	// Funktionsname rausziehen
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "identifier") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Init Option expects a function call, no function name found here!"));
	}
	fname = Tok.getIdentifierInfo()->getName().str();
	fnameFoundAt = Tok.getLocation();
}

void FuzzPragmaAttrInline::parse_position(clang::Token &Tok)
{
	PP.Lex(Tok);
	if(strcmp(Tok.getName(), "identifier") != 0)
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Inline Option requires a second Parameter!"));
	}
	auto posval = Tok.getIdentifierInfo()->getName().str();
	if(posval == "before")
	{
		position = CallPos::Before;
	}
	else if(posval == "after")
	{
		position = CallPos::After;
	}
	else if(posval == "replace")
	{
		position = CallPos::Replace;
	}
	else if(posval == "fullbody")
	{
		position = CallPos::FullBody;
	}
	else
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Second argument of Inline Option should be either before, after or replace!"));
	}
}

std::vector<clang::Stmt*> FuzzPragmaAttrInline::getInlineStmts(const std::vector<clang::FunctionDecl *> &functionDecls)
{
	std::vector<clang::Stmt *> stmts;
	fillInlineStmts(functionDecls, stmts);

	FuzzPragmaAttrInline *curNode = this;
	while((curNode = curNode->nextNode) != nullptr)
	{
		curNode->fillInlineStmts(functionDecls, stmts);
	}

	return stmts;
}

void FuzzPragmaAttrInline::fillInlineStmts(const std::vector<clang::FunctionDecl*> &functionDecls, std::vector<clang::Stmt*> &stmts)
{
	clang::FunctionDecl* target = findFunction(fname, functionDecls, true);

	if(!target)
	{
		clang::DiagnosticBuilder DB = DE.Report(fnameFoundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Function that should be called was not found!"));
	}
	else
	{
		for(clang::Stmt::child_iterator i = ((clang::CompoundStmt*) target->getBody())->child_begin(), e = ((clang::CompoundStmt*) target->getBody())->child_end(); i != e; i++)
		{
			stmts.push_back(*i);

			if((*i)->getStmtClass() == clang::Stmt::StmtClass::DeclRefExprClass)
			{
				if(((clang::DeclRefExpr*) *i)->getReferencedDeclOfCallee()->getKind() == clang::Decl::Kind::Var)
				{
					funcvars.push_back((clang::VarDecl*)((clang::DeclRefExpr*) *i)->getReferencedDeclOfCallee());
				}
			}
		}
	}
}

std::vector<clang::VarDecl*> FuzzPragmaAttrInline::getFuncVars()
{
	return funcvars;
}