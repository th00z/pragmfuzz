/**
 * @author Lennart Haas
 * @see https://bsc.sciencetodo.org/
 */

#include "FuzzPragmaAttributes.h"

std::unique_ptr<CallTypeInfo> FuzzPragmaAttribute::buildParamInfo(clang::SourceManager &SM, clang::Token &Tok, clang::Preprocessor &PP)
{
	clang::DiagnosticsEngine &DE = PP.getDiagnostics();
	std::unique_ptr<CallTypeInfo> info(new CallTypeInfo);

	clang::SourceLocation lStart = Tok.getLocation();
	clang::SourceLocation lEnd = Tok.getEndLoc();
	std::string data = std::string(SM.getCharacterData(lStart), SM.getCharacterData(lEnd) - SM.getCharacterData(lStart));

	info->foundAt = lStart;

	if(strcmp(Tok.getName(), "numeric_constant") == 0)
	{
		if(data.find('.') != std::string::npos)
		{
			info->type = "double";
			info->val = data;
		}
		else
		{
			info->type = "int";
			info->val = data;
		}
	}
	else if(!strcmp(Tok.getName(), "string_literal"))
	{
		info->type = "string";
		info->val = data;
	}
	else if(!strcmp(Tok.getName(), "char_constant"))
	{
		info->type = "char";
		info->val = data;
	}
	else if(!strcmp(Tok.getName(), "identifier"))
	{
		if(strcmp(PP.LookAhead(0).getName(), "l_paren") == 0)
		{
			info->type = "func";
			info->val = data;
			PP.Lex(Tok);
			PP.Lex(Tok);
		}
		else
		{
			if ((data.find("true") != std::string::npos) || (data.find("TRUE") != std::string::npos) || (data.find("True") != std::string::npos))
			{
				info->type = "bool";
				info->val = "true";
			}
			else if ((data.find("false") != std::string::npos) || (data.find("FALSE") != std::string::npos) || (data.find("False") != std::string::npos))
			{
				info->type = "bool";
				info->val = "false";
			}
			else if ((data.find("null") != std::string::npos) || (data.find("NULL") != std::string::npos) || (data.find("Null") != std::string::npos))
			{
				info->type = "nullptr";
				info->val = data;
			}
			else if ((data.find("call")) != std::string::npos)
			{
			}
			else
			{
				info->type = "var";
				info->val = data;
			}
		}
	}
	else
	{
		clang::DiagnosticBuilder DB = DE.Report(Tok.getLocation(), DE.getCustomDiagID(clang::DiagnosticsEngine::Warning, "Unknown parameter type: '%0' : '%1'"));
		DB.AddString(Tok.getName());
		DB.AddString(data);
	}

	return info;
}

std::vector<clang::Expr*> FuzzPragmaAttribute::buildArgVec(std::vector<std::unique_ptr<CallTypeInfo>> paramInfo, clang::ASTContext &context, std::map<std::string, clang::ParmVarDecl*> vars, const std::vector<clang::FunctionDecl *> &functionDecls, clang::DiagnosticsEngine &DE)
{
	std::vector<clang::Expr*> ArgInfo;
	for(const auto &elem : paramInfo)
	{
		if(elem->type == "int")
		{
			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
			  context,
			  context.IntTy.getNonReferenceType(),
			  clang::CK_LValueToRValue,
			  clang::IntegerLiteral::Create(context, llvm::APInt(32, (uint64_t)std::stoi(elem->val), true), context.IntTy, clang::SourceLocation()),
			  nullptr,
			  clang::VK_RValue
			));
		}
		else if(elem->type == "double")
		{
			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
			  context,
			  context.DoubleTy.getNonReferenceType(),
			  clang::CK_LValueToRValue,
			  clang::FloatingLiteral::Create(context, llvm::APFloat(std::stod(elem->val)), false, context.DoubleTy, clang::SourceLocation()),
			  nullptr,
			  clang::VK_RValue
			));
		}
		else if(elem->type == "string")
		{
			str_replace(elem->val, "\"", "");

			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
			  context,
			  context.CharTy.getNonReferenceType(),
			  clang::CK_LValueToRValue,
			  clang::StringLiteral::Create(context, elem->val, clang::StringLiteral::StringKind::UTF8, false, context.CharTy, clang::SourceLocation()),
			  nullptr,
			  clang::VK_RValue
			));
		}
		else if(elem->type == "char")
		{
			str_replace(elem->val, "'", "");

			auto *charexpr = new (context) clang::CharacterLiteral((unsigned int)elem->val[0], clang::CharacterLiteral::CharacterKind::UTF8, context.CharTy, clang::SourceLocation());

			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
			  context,
			  context.CharTy.getNonReferenceType(),
			  clang::CK_LValueToRValue,
			  charexpr,
			  nullptr,
			  clang::VK_RValue
			));
		}
		else if(elem->type == "bool")
		{
			auto *boolexpr = new (context) clang::CXXBoolLiteralExpr(true, context.BoolTy, clang::SourceLocation());

			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
					context,
					context.BoolTy.getNonReferenceType(),
					clang::CK_LValueToRValue,
					boolexpr,
					nullptr,
					clang::VK_RValue
			));
		}
		else if(elem->type == "nullptr")
		{
			auto *nullexpr = new (context) clang::CXXNullPtrLiteralExpr(context.NullPtrTy, clang::SourceLocation());

			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
				context,
				context.NullPtrTy.getNonReferenceType(),
				clang::CK_LValueToRValue,
				nullexpr,
				nullptr,
				clang::VK_RValue
			));
		}
		else if(elem->type == "var")
		{
			if(vars.find(elem->val) == vars.end())
			{
				clang::DiagnosticBuilder DB = DE.Report(elem->foundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Variable %0 not found!"));
				DB.AddString(elem->val);
				return ArgInfo;
			}
			if(getAttrType() == "Init")
			{
				clang::DiagnosticBuilder DB = DE.Report(elem->foundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Warning, "Using variables in Init function call is bad practice as the init function is invoked first in the fuzz target!"));
			}
			ArgInfo.push_back(clang::ImplicitCastExpr::Create
			(
			  context,
			  vars[elem->val]->getType().getNonReferenceType(),
			  clang::CK_LValueToRValue,
			  clang::DeclRefExpr::Create
			  (
				  context,
				  clang::NestedNameSpecifierLoc(),
				  clang::SourceLocation(),
				  vars[elem->val],
				  false,
				  clang::SourceLocation(),
				  vars[elem->val]->getType().getNonReferenceType(),
				  clang::VK_LValue
			  ),
			  nullptr,
			  clang::VK_RValue
			));
		}
		else if(elem->type == "func")
		{
			clang::FunctionDecl* target = findFunction(elem->val, functionDecls, false);
			if(target)
			{
				ArgInfo.push_back(new (context) clang::CallExpr
				(
				  context,
				  clang::ImplicitCastExpr::Create
				  (
					  context,
					  target->getReturnType().getNonReferenceType(),
					  clang::CK_LValueToRValue,
					  clang::ImplicitCastExpr::Create
					  (
						  context,
						  target->getReturnType().getNonReferenceType(),
						  clang::CK_FunctionToPointerDecay,
						  clang::DeclRefExpr::Create
						  (
							  context,
							  clang::NestedNameSpecifierLoc(),
							  clang::SourceLocation(),
							  target,
							  false,
							  clang::SourceLocation(),
							  target->getReturnType().getNonReferenceType(),
							  clang::VK_LValue
						  ),
						  nullptr,
						  clang::VK_LValue
					  ),
					  nullptr,
					  clang::VK_RValue
				  ),
				  llvm::ArrayRef<clang::Expr*>(),
				  target->getReturnType().getNonReferenceType(),
				  clang::VK_LValue,
				  clang::SourceLocation()
				));
			}
			else
			{
				clang::DiagnosticBuilder DB = DE.Report(elem->foundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Function that should be called was not found!"));
			}
		}
		else
		{
			clang::DiagnosticBuilder DB = DE.Report(elem->foundAt, DE.getCustomDiagID(clang::DiagnosticsEngine::Fatal, "Invalid argument type!"));
		}
	}
	return ArgInfo;
}

clang::FunctionDecl* FuzzPragmaAttribute::findFunction(std::string fname, const std::vector<clang::FunctionDecl *> &functionDecls, bool withBody)
{
	for(const auto &f : functionDecls)
	{
		if(f->getName().str() == fname)
		{
			if(!withBody || f->hasBody())
			{
				return f;
			}
		}
	}
	return (clang::FunctionDecl*) nullptr;
}

void FuzzPragmaAttribute::str_replace(std::string &str, const std::string &from, const std::string &to)
{
	size_t start_pos = 0;
	while((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
}