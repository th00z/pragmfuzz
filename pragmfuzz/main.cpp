/**
 * @author Sirko Höer
 * @author Lennart Haas (parts marked)
 */

#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include "PPContext.h"
#include "FuzzPragmaAttributes.h"
#include "FuzzPragmaHandler.h"
#include "PragmaReaderAction.h"
#include <vector>
//#include <iostream>

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

static llvm::cl::OptionCategory tool_category("pragma-reader-options");

template<typename T>
std::ostream &operator<<(std::ostream &out, const std::vector<T> &vec) {
	out << "vector:\n";
	for (const auto &elem : vec)
		out << '\t' << &elem << '\n';
	return out;
}

/// @author Lennart Haas
template<typename T>
std::ostream &operator<<(std::ostream &out, const unique_ptr<pragmaInfo> &info)
{
	out << "pragmaInfo;\n";
	out << &info->location << "\n";
	out << info->pragmaParams;
	return out;
}

void setHeaders(PPContext &ppC)
{
	ppC.setHeaderPath("/usr/local/include");
	ppC.setHeaderPath("/usr/lib/llvm-6.0/lib/clang/6.0.1/include");
	ppC.setHeaderPath("/usr/include/x86_64-linux-gnu");
	ppC.setHeaderPath("/usr/include");
	ppC.setHeaderPath("/usr/lib/gcc/x86_64-linux-gnu/6/include");
	ppC.setHeaderPath("/usr/include/c++/6.3.0");
	ppC.setHeaderPath("/usr/include/x86_64-linux-gnu/c++/6.3.0");
	ppC.setHeaderPath("/usr/include/c++/6.3.0/backward");
	ppC.setHeaderPath("/usr/include/clang/6.0.1/include");
	ppC.setHeaderPath("/usr/local/include/");
}

int main(int argc, const char **argv)
{

	//Initial CommonOptionParser with the args from the commandline
	CommonOptionsParser OptionsParser(argc, argv, tool_category);
	ClangTool Tool(OptionsParser.getCompilations(),
				   OptionsParser.getSourcePathList());

	// create und initialize the essential components for traversing an AST
	PPContext ppC{Tool, OptionsParser};
	setHeaders(ppC);
	ppC.createPreprocessor();

	auto *pragmaHandler = new FuzzPragmaHandler(ppC.getCompilerInstance().getPreprocessor()); // Line added by Lennart Haas
	ppC.addPragmaHandler("fuzz", pragmaHandler); // Line added by Lennart Haas

	ppC.createASTContext();

	// Create a AST-Consumer
	PragmaReaderASTConsumer TheASTConsumer(pragmaHandler); // pragmaHandler-Attribute added by Lennart Haas

	// Set the AST-Consumer to the compiler instanze
	PragmaReaderAction pragma_read_definition(ppC);

	// start find location ...
	pragma_read_definition.run(&TheASTConsumer);

	pragmaHandler->dump(); // Line added by Lennart Haas
	pragmaHandler->process(); // Line added by Lennart Haas

	//ppC.getCompilerInstance().getASTContext().getTranslationUnitDecl()->dumpColor();

	//auto *TopDecl = ppC.getCompilerInstance().getASTContext().getTranslationUnitDecl();
	//TopDecl->dumpColor();

	return 0;
}
